package structure;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class KnowledgeBase {
	
	protected FactBase baseFait;
	protected FactBase baseFaitSaturee;
	protected RuleBase baseRegle;
	
	protected ArrayList<Atom> proved;
	protected ArrayList<Atom> provedWrong;
	
	protected ArrayList<Atom> proving;
	
	public ArrayList<Atom> getProved() {
		return proved;
	}

	public void setProved(ArrayList<Atom> proved) {
		this.proved = proved;
	}

	public ArrayList<Atom> getProvedWrong() {
		return provedWrong;
	}

	public void setProvedWrong(ArrayList<Atom> provedWrong) {
		this.provedWrong = provedWrong;
	}

	
	public KnowledgeBase(FactBase fb, RuleBase rb) {
		this.baseFait = fb;
		this.baseRegle = rb;
		this.baseFaitSaturee = new FactBase();
		this.baseFaitSaturee.addAtoms(this.baseFait.getAtoms());
		
	}
	
	public KnowledgeBase() {
		this.baseFait = new FactBase();
		this.baseFaitSaturee = new FactBase();
		this.baseRegle = new RuleBase();
	}
	
	public KnowledgeBase(String path) {
		this.baseFaitSaturee = new FactBase();
		this.baseRegle = new RuleBase();
		try {
			System.out.println("Chargement du fichier : "+ new java.io.File(".").getCanonicalPath()+ "/" + path);
			BufferedReader readFile = new BufferedReader(new FileReader (path));
			
			System.out.println("Lecture du fichier " + path);
			
			String s = readFile.readLine();
			
			this.baseFait = new FactBase(s);
			
			while((s = readFile.readLine()) != null && s.length() != 0) {
				this.baseRegle.addRule(new Rule(s));
				
				System.out.println(s);
			}
			readFile.close();
		}catch(IOException e) {
		 System.out.println("Erreur : " + e.getMessage());
		}
		
		this.baseFaitSaturee.addAtoms(this.baseFait.getAtoms());
	}
	
	public RuleBase getBaseRegle() {
		return this.baseRegle;
	}
	
	public FactBase getBaseFait() {
		return this.baseFait;
	}
	
	public FactBase getBaseFaitSaturee() {
		return this.baseFaitSaturee;
	}
	
	public String toString() {
		String BF = this.baseFait.toString();
		String BFS = this.baseFaitSaturee.toString();
		String BR = this.baseRegle.toString();
		return ("BF : " + BF + "\nBR : " + BR + "\n BF Saturée : " + BFS);
	}
	
	public void ForwardChaining() {
		
		boolean end = false;
		
		ArrayList<Boolean> applied = new ArrayList<>();
		for(int i = 0; i < this.baseRegle.size(); i++){
			applied.add(false);
		}
		
		ArrayList<Atom> newFacts = new ArrayList<>();
		int ruleNumber = 0;
		Rule currentRule;
		while(!end) {
			newFacts.clear();
			
			for(int i = 0; i < this.baseRegle.size() ; i++) {
				if(!applied.get(i)) {
					currentRule = this.baseRegle.getRule(i);
					for(int j = 0 ; j < currentRule.getHypothesis().size() ; j++) {
						if(this.baseFaitSaturee.getAtoms().containsAll(currentRule.getHypothesis())){
							applied.set(i, true);
							//System.out.println(applied);
							if(!baseFaitSaturee.getAtoms().contains(currentRule.getConclusion()) && !newFacts.contains(currentRule.getConclusion())) {
								newFacts.add(currentRule.getConclusion());
							}
							
						}
					}
				}
			}
			if(newFacts.size() == 0) {
				end = true;
			}else {
				this.baseFaitSaturee.addAtoms(newFacts);
			}
			
			
		}
		System.out.println("BFS : " + this.baseFaitSaturee);
		
		
	}
	
	public void forwardChainingOpti() {
		ArrayList<Atom> aTraiter =  new ArrayList<>();
		for(Atom a : this.baseFait.getAtoms()) {
			aTraiter.add(a);
		}
		
		HashMap<Rule,Integer> counter = new HashMap<>();
		
		for(Rule r : this.baseRegle.getRules()) {
			counter.put(r, r.getHypothesis().size());
		}
		
		while(aTraiter.size() > 0) {
			Atom current = aTraiter.remove(0);
			
			for(Rule r : this.baseRegle.getRules()) {
				if(r.getHypothesis().contains(current) && counter.get(r) != 0) {
					counter.put(r,counter.get(r)-1);
					
					if(counter.get(r) == 0) {
						ArrayList<Atom> BFS = this.baseFaitSaturee.getAtoms();
						if(!BFS.contains(r.getConclusion()) && !aTraiter.contains(r.getConclusion())) {
							aTraiter.add(r.getConclusion());
							this.baseFaitSaturee.addAtomWithoutCheck(r.getConclusion());
						}
					}
				}
			}
			
		}
		System.out.println("BFS : " + this.baseFaitSaturee);
	}
	
	private ArrayList<Rule> getRules(Atom a){
		ArrayList<Rule> rules = new ArrayList<>();
		for(Rule r : this.baseRegle.getRules()) {
			if(r.getConclusion().equals(a)) {
				rules.add(r);
			}
		}
		
		return rules;
	}
	
	public boolean BackwardChaining(Atom a,ArrayList<Atom> L) {
		ArrayList<Rule> rules;
		ArrayList<Atom> currentL = (ArrayList<Atom>)L.clone();
		currentL.add(a);
		if(this.baseFait.getAtoms().contains(a)) {
			
			return true;
		}
		int i = 0;
		for(Rule r : getRules(a)) {
			boolean aucun = true;
			for(Atom h : r.getHypothesis()) {
				if(L.contains(h)) {
					aucun = false;
				}
				
			}
			if(aucun) {
				i = 0;
				while(i < r.getHypothesis().size() && BackwardChaining(r.getAtomHyp(i),currentL) ) {
					i++;
				}
				
				if(i == r.getHypothesis().size()) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean backwardChainingLl(Atom Q, HashSet<Atom> L) {
		//Rule r;
		int i, n;
		HashSet<Atom> LApp = (HashSet<Atom>)L.clone();
		LApp.add(Q);
		
		if(this.baseFait.belongsAtom(Q)) {
			return true;
		}
		
		for(Rule r : getRules(Q)) {
			boolean noOne = true;
//			r = this.baseRegle.getRule(k);
			n = r.getHypothesis().size();
			for(Atom H : r.getHypothesis()) {
				if(L.contains(H)) {
					noOne = false;
				}
			}
			
			if(noOne) {
				i = 0;
					
				while(i < n && backwardChainingLl(r.getAtomHyp(i),LApp)) {
					i++;
				}
				
				if(i >= n) {
					return true;
				}
				
			}
		}
		
		return false;
	}
	
	public boolean backwardChainingTree(Atom a,ArrayList<Atom> L,int depth) {
		if(depth == 0 ) {
			System.out.println(a);
		}
		ArrayList<Rule> rules;
		ArrayList<Atom> currentL = (ArrayList<Atom>)L.clone();
		currentL.add(a);
		if(this.baseFait.getAtoms().contains(a)) {

			for(int x = 0;x < depth; x++) {
				System.out.print("---");
			}
			System.out.println(a.toString() + " ∈ BF");
			return true;
		}
		int i = 0;
		for(Rule r : getRules(a)) {
			boolean aucun = true;
			for(Atom h : r.getHypothesis()) {
				if(L.contains(h)) {
					aucun = false;
				}
				
			}
			if(aucun) {
				i = 0;
				while(i < r.getHypothesis().size() && backwardChainingTree(r.getAtomHyp(i),currentL,depth+1) ) {
					i++;
				}
				
				if(i == r.getHypothesis().size()) {
					for(int x = 0;x < depth; x++) {
						System.out.print("---");
					}
					System.out.println(a.toString() + " est prouvé");
					
					return true;
				}
			}
		}
		for(int x = 0;x < depth; x++) {
			System.out.print("---");
		}
		System.out.println(a.toString() + " : échec");

		return false;
	}
	
	public boolean backwardChainingOpti(Atom a,ArrayList<Atom> L,int depth) {
		
		if(depth == 0) {
			System.out.println(a);
			this.proved = new ArrayList<>();
			this.provedWrong = new ArrayList<>();
			this.proving = new ArrayList<>();
			this.proved.addAll(this.baseFait.getAtoms());
		}
		
		if(proving.contains(a)) {
			for(int x = 0;x < depth; x++) {
				System.out.print("---");
			}
			System.out.print(a.toString() + " : cycle");
		}
		
		ArrayList<Rule> rules;
		ArrayList<Atom> currentL = (ArrayList<Atom>)L.clone();
		currentL.add(a);
		if(this.baseFait.getAtoms().contains(a) || proved.contains(a)) {
			for(int x = 0;x < depth; x++) {
				System.out.print("---");
			}
			System.out.print(a.toString());
			
			if(!proved.contains(a)) {
				proved.add(a);
				System.out.println(" est prouvé");
			}else {
				System.out.println(" ∈ DP");
			}
			return true;
		}
		if(provedWrong.contains(a)) {
			for(int x = 0;x < depth; x++) {
				System.out.print("---");
			}
			System.out.println(a.toString() + " déjà faux");
			return false;
		}
		
		int i = 0;
		for(Rule r : getRules(a)) {
			boolean aucun = true;
			for(Atom h : r.getHypothesis()) {
				if(L.contains(h)) {
					aucun = false;
				}
				
			}
			if(aucun) {
				i = 0;
				
				proving.add(a);
				while(i < r.getHypothesis().size() && backwardChainingOpti(r.getAtomHyp(i),currentL,depth+1) ) {
					i++;
				}
				
				if(i == r.getHypothesis().size()) {
					for(int x = 0;x < depth; x++) {
						System.out.print("---");
					}
					System.out.print(a.toString());

					if(!proved.contains(a)) {
						proved.add(a);
						System.out.println(" est prouvé");
					}else {
						System.out.println(" ∈ DP");
					}
					return true;
				}
			}
		}
		for(int x = 0;x < depth; x++) {
			System.out.print("---");
		}
		System.out.print(a.toString());
		if(!provedWrong.contains(a)) {
			provedWrong.add(a);
			System.out.println(" est faux");
		}else {
			System.out.println(" est déjà faux");
		}
		
		return false;
	}
	
}
